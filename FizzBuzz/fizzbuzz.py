def fizz_buzz(x):
    if x % 3 == 0 and x % 5 == 0:
        print("fizzbuzz")
    elif x % 3 == 0:
        print("fizz")
    elif x % 5 == 0:
        print("buzz")


for i in range(1, 21):
    print(i), fizz_buzz(i)

'''
# Shortest Fizzbuzz https://code.sololearn.com/cn6fJ65with0/#py
for x in range(21):
   print("Fizz"[x % 3 * 4:] + "Buzz"[x % 5 * 4:] or x)
'''
