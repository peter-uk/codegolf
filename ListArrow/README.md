# ListArrow

 Exercise! Display the image below to the right hand side where the 0 is going to be ' ', and the 1 is going to be 
    '*'. This will reveal an image! 


## Output

```
      *       
    * * *     
  * * * * *   
* * * * * * * 
      *       
      *       
```
