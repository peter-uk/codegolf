picture = [
    [0, 0, 0, 1, 0, 0, 0],
    [0, 0, 1, 1, 1, 0, 0],
    [0, 1, 1, 1, 1, 1, 0],
    [1, 1, 1, 1, 1, 1, 1],
    [0, 0, 0, 1, 0, 0, 0],
    [0, 0, 0, 1, 0, 0, 0]
]

# Loops through each list 01,02,03 - 11,12,13 etc
for row in picture:
    for item in row:
        if item:
            print('*', end=' ')
        else:
            print(' ', end=' ')
    print("")
