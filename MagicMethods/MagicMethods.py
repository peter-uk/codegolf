class Toy:
    def __init__(self, color, age):
        self.color = color
        self.age = age
        self.my_dict = {
            'name': 'Yoyo',
            'has_pets': False,
        }

    def __str__(self):
        return f"{self.color}"

    # sets length 5
    def __len__(self):
        return 5

    def __del__(self):
        return "deleted"

    def __call__(self):
        return 'yes??'

    def __getitem__(self, i):
        return self.my_dict[i]


action_figure = Toy('red', 0)
print(action_figure.__str__())  # Calls __str__ returns red
print(str(action_figure))       # Calls __str__ returns red
print(len(action_figure))       # returns 5
print(action_figure())          # calls function but its overwritten with return yes??
print(action_figure['name'])    # uses __getitem__
print(action_figure.__del__())  # uses __del__
