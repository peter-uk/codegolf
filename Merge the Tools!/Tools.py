def merge_the_tools(string, k):
    # Splits string into a list given k
    split = list(map(''.join, zip(*[iter(string)] * (int(len(string) / k)))))
    for c in split:  # Loops through list
        print(''.join(set(c)))  # Join set C to AB instead of 'A' 'B'


if __name__ == '__main__':
    merge_the_tools("AABCAAADA", 3)
