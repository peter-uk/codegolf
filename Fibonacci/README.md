# Fibonacci Sequence

In mathematics, the Fibonacci numbers, commonly denoted Fn, form a sequence, called the Fibonacci sequence, such that each number is the sum of the two preceding ones, starting from 0 and 1:

````
The beginning of the sequence is this:

0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144
````

## Output

```
fib(5) ➞ 5

fib(10) ➞ 55

fib(20) ➞ 6765

fib(50) ➞ 12586269025
```

