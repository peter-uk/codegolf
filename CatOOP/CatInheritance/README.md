# Cat Inheritance 

    1. Add another Cat
    2. Create a list of all of the pets (create 3 cat instances from the above)
    3. Instantiate the Pet class with all your cats use variable my_pets
    4. 4 Output all of the cats walking using the my_pets instance


## Output

```
Paul is just walking around
Sally is just walking around
Simon is just walking around
```
