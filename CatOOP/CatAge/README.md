# Cat Age

    1. Instantiate the Cat object with 3 cats
    2. Create a function that finds the oldest cat
    3. Print out: "The oldest cat is x years old.". x will be the oldest cat age by using the function in #2


## Output

```
The oldest cat is x years old.
```
