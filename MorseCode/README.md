# Encode Morse
Create a function that takes a string as an argument and returns the Morse code equivalent.
https://edabit.com/challenge/5bYXQfpyoithnQisa

## Output

```
encode_morse("EDABBIT CHALLENGE") ➞ ". -.. .- -... -... .. -   -.-. .... .- .-.. .-.. . -. --. ."

encode_morse("HELP ME !") ➞ ".... . .-.. .--.   -- .   -.-.--"
```

