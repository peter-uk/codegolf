# Find Domain Name From DNS Pointer (PTR) Records Using IP Address

https://edabit.com/challenge/MtktG9Dz7z9vBCFYM

## Output

```
get_domain("8.8.8.8") ➞ "dns.google"

get_domain("8.8.4.4") ➞ "dns.google"
```

